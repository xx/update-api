package model

import "github.com/go-git/go-git/v5"

type Repo struct {
	Name     string
	URL      string
	Plumbing *git.Repository
}
