package model

type Commit struct {
	ID          string `json:"commit"`
	Description string `json:"description"`
}

type Changelog struct {
	Latest  string    `json:"latest"`
	Changes *[]Commit `json:"changes"`
}
