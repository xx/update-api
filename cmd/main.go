package main

import (
	"flag"
	"fmt"
	"os"
	"path"

	v1 "git.dog/xx/update-api/api/v1"
	"git.dog/xx/update-api/config"
	"git.dog/xx/update-api/gitrepo"
	"git.dog/xx/update-api/log"
	"git.dog/xx/update-api/model"
	"github.com/gin-gonic/gin"
)

func main() {
	conf, err := parseCommandLineArgs()
	if err != nil {
		fmt.Printf("failed to start: %s\nsee -help for usage\n", err.Error())
		os.Exit(1)
	}

	ret, err := runApp(conf)
	if err != nil {
		fmt.Printf("application failed to run: %s\n", err.Error())
	}

	os.Exit(ret)
}

func parseCommandLineArgs() (*config.Config, error) {
	clean := flag.Bool("f", false, "clean local repositories")
	configPath := flag.String("c", "", "path of config file to use")
	flag.Parse()

	conf, err := config.LoadConfigOrFindDefault(*configPath)
	if err != nil {
		return nil, err
	}

	conf.Clean = *clean

	return conf, nil
}

func runApp(conf *config.Config) (int, error) {
	err := log.SetSeverity(conf.LogLevel)

	if err != nil {
		return 1, fmt.Errorf("failed to load config file: %s", err.Error())
	}

	for repoName, repoMeta := range conf.Repos {
		repo := model.Repo{
			Name: repoName,
			URL:  repoMeta.URL,
		}

		repoAlreadyExists, err := gitrepo.SetUpGit(&repo, path.Join(conf.RepoPath, repoName), conf.Clean)
		if err != nil {
			return 2, fmt.Errorf("failed to clone git repo: %s", err.Error())
		}

		if repoAlreadyExists && repoMeta.UpdateOnStartup {
			_, err = gitrepo.UpdateRepo(repo.Plumbing)
			if err != nil {
				return 2, err
			}
			log.Debug("clean requested for %s", repoName)
		}

		go gitrepo.SleepAndPull(&repo, repoMeta.UpdateInterval)
		err = v1.SetRepo(&repo)
		if err != nil {
			return 5, err
		}

		log.Debug("set up project %s to track url %s", repoName, repo.URL)
	}

	err = runGin(conf.ListenHost, conf.ListenPort)
	if err != nil {
		return 3, fmt.Errorf("gin error: %s", err.Error())

	}

	return 0, nil
}

func runGin(host string, port int) (err error) {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()

	router.Use(log.GinJsonLogger())
	router.Use(gin.Recovery())

	err = router.SetTrustedProxies(nil)
	if err != nil {
		return
	}

	v1Group := router.Group("/v1")
	v1.RegisterHandlers(v1Group)

	log.Debug("everything seems ready, starting gin")
	err = router.Run(fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		return err
	}

	return
}
