package log

type ginJsonLogEntry struct {
	Status   int    `json:"status"`
	Duration string `json:"duration"`
	ClientIP string `json:"client_ip"`
	Method   string `json:"method"`
	Path     string `json:"path"`
}

type logEntry struct {
	Time     string      `json:"time"`
	Severity string      `json:"severity"`
	Source   string      `json:"source"`
	Message  interface{} `json:"message"`
}
