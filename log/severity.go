package log

import "fmt"

type Severity struct {
	weight int
	name   string
}

var (
	DEBUG   = Severity{0, "debug"}
	INFO    = Severity{1, "info"}
	WARNING = Severity{2, "warning"}
	ERROR   = Severity{3, "error"}
	FATAL   = Severity{4, "fatal"}
)

var severityMap = map[string]Severity{
	"debug":   DEBUG,
	"info":    INFO,
	"warning": WARNING,
	"error":   ERROR,
	"fatal":   FATAL,
}

func GetSeverity(level string) (Severity, error) {
	if sev, ok := severityMap[level]; ok {
		return sev, nil
	}

	return DEBUG, fmt.Errorf("no such log severity: %s", level)
}

func SetSeverity(level string) error {
	var err error
	LogSeverity, err = GetSeverity(level)

	if err == nil {
		Debug("setting logger severity to %s", LogSeverity.name)
	} else {
		Debug("attempted to set logger severity, but: %s", err.Error())
	}

	return err
}
