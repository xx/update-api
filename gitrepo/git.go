package gitrepo

import (
	"errors"
	"fmt"
	"os"
	"time"

	"git.dog/xx/update-api/log"
	"git.dog/xx/update-api/model"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
)

func SetUpGit(repo *model.Repo, path string, clean bool) (bool, error) {
	if clean {
		err := os.RemoveAll(path)
		if err != nil {
			log.Error("a clean was requested, but was unable to delete files: %s", err.Error())
			return false, err
		}
	} else {
		stat, err := os.Stat(path)
		if !os.IsNotExist(err) && stat.IsDir() {
			r, err := git.PlainOpen(path)
			repo.Plumbing = r
			return true, err
		}
	}

	if repo.URL == "" {
		return false, errors.New("neither local repo nor repo url set")
	}

	r, err := git.PlainClone(path, true, &git.CloneOptions{
		URL: repo.URL,
	})

	repo.Plumbing = r

	return false, err
}

func UpdateRepo(repo *git.Repository) (bool, error) {
	err := repo.Fetch(&git.FetchOptions{
		RemoteName: "origin",
	})

	if err == git.NoErrAlreadyUpToDate {
		return false, nil
	}

	if err != nil {
		return false, fmt.Errorf("failed to fetch changes: %s", err.Error())
	}

	ref, err := repo.Head()
	if err != nil {
		return true, fmt.Errorf("failed to get HEAD: %s", err.Error())
	}

	latest, err := repo.ResolveRevision(plumbing.Revision("origin/" + ref.Name().Short()))
	if err != nil {
		return true, fmt.Errorf("failed to get latest HEAD: %s", err.Error())
	}

	newBranch := plumbing.NewHashReference(ref.Name(), *latest)
	err = repo.Storer.SetReference(newBranch)
	if err != nil {
		return true, fmt.Errorf("failed to update branch: %s", err.Error())
	}

	headRef := plumbing.NewSymbolicReference(plumbing.HEAD, newBranch.Name())
	err = repo.Storer.SetReference(headRef)
	if err != nil {
		return true, fmt.Errorf("failed to update HEAD: %s", err.Error())
	}

	return true, nil
}

func SleepAndPull(repo *model.Repo, timeout int) {
	if timeout <= 0 {
		return
	}

	log.Debug("initialized periodic sync for: %s", repo.Name)

	for {
		time.Sleep(time.Duration(timeout) * time.Second)
		log.Debug("starting periodic sync for: %s", repo.Name)

		hadUpdate, err := UpdateRepo(repo.Plumbing)
		if err != nil {
			message := fmt.Sprintf("syncing repo %s failed", repo.Name)
			if hadUpdate {
				message += " (repo might be in an unstable state)"
			}
			message += fmt.Sprintf(": %s", err.Error())
			log.Error(message)
			continue
		}

		if hadUpdate {
			log.Info("pulled changes for repo: %s", repo.Name)
		}
	}
}
