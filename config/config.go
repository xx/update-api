package config

import (
	"errors"
	"os"
	"path"

	"git.dog/xx/update-api/log"
	"github.com/BurntSushi/toml"
)

var (
	// config file paths from most to least important
	configPaths = []string{
		os.Getenv("UPDATE_API_CONFIG"),
		path.Join(os.Getenv("HOME"), ".config/update-api/config.toml"),
		"/etc/update-api/config.toml",
	}
)

type (
	Config struct {
		ListenHost string          `toml:"listen-host"`
		ListenPort int             `toml:"listen-port"`
		RepoPath   string          `toml:"repo-path"`
		LogLevel   string          `toml:"log-level"`
		Repos      map[string]repo `toml:"repos"`
		Clean      bool            `toml:"clean"`
	}

	repo struct {
		URL             string `toml:"url"`
		UpdateInterval  int    `toml:"update-interval"`
		UpdateOnStartup bool   `toml:"update-on-startup"`
	}
)

func LoadConfig(configPath string) (*Config, error) {
	var conf Config
	_, err := toml.DecodeFile(configPath, &conf)
	return &conf, err
}

func TryCommonConfigPaths() (*Config, error) {
	for _, confPath := range configPaths {
		_, err := os.Stat(confPath)
		if err == nil {
			// this will probably neve be printed
			log.Debug("found config at %s, loading it", confPath)
			return LoadConfig(confPath)
		}
	}

	return nil, errors.New("no config file found")
}

func LoadConfigOrFindDefault(configPath string) (*Config, error) {
	if configPath != "" {
		return LoadConfig(configPath)
	}
	return TryCommonConfigPaths()
}
